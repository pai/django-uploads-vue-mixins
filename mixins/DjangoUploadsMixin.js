import BMF from 'browser-md5-file/dist/index.esm'

export default {
  data () {
    return {
      bmf: new BMF(),

      uploadObject: null
    }
  },
  computed: {
    maxFileSize () {
      if (this.backendInstanceStats) {
        return (this.backendInstanceStats.max_file_size_mb)
      }
      return 0
    },
    downloadLink () {
      if (this.uploadObject && this.uploadObject.links && this.uploadObject.links.length) {
        return this.uploadObject.links[0].download_link
      }
    }
  },
  methods: {
    fileSizeMb (file) {
      let size = 0
      if (file) {
        size = file.size
      }
      return this.humanFileSize(size)
    },
    checkFileSize (file) {
      if (file.size > (this.maxFileSize * (1014*1024))) {
        throw 'maxFileSizeExceeded'
      }
    },
    checkFile (file) {
      this.checkFileSize(file)
    },
    humanFileSize (bytes, si) {
      var thresh = si ? 1000 : 1024
      if(Math.abs(bytes) < thresh) {
          return bytes + ' B'
      }
      var units = si
          ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
          : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB']
      var u = -1
      do {
          bytes /= thresh
          ++u
      } while(Math.abs(bytes) >= thresh && u < units.length - 1)
      return bytes.toFixed(1) + ' ' + units[u]
    },
    calculateMd5Sum (file) {
      return new Promise((resolve, reject) => {
        this.bmf.md5(
          file,
          (err, md5) => {
            if (err) {
              reject(err)
            } else {
              resolve(md5)
            }
          },
          progress => {
            this.$emit('fileMd5Progress', progress)
          }
        )
      })
    },
    reserveUpload (url, file, upload_type, options) {
      if (options === undefined) {
        options = {
            comment: null,
            meta: null
        }
      }

      return new Promise((resolve, reject) => {
        let file_mime = 'application/octet-stream'

        if (file.type && file.type !== '') {
          file_mime = file.type
        }

        this.calculateMd5Sum(file).then(
          (md5) => {
            let data = {
              upload_type: upload_type,
              file_name: file.name,
              file_mime: file_mime,
              file_md5: md5,
              comment: options.comment,
              meta: options.meta
            }

            this.$http.post(url,
              data,
              {
                withCredentials: true,
              }).then((resp) => {
                this.uploadObject = resp.data
                resolve(this.uploadObject)
              }).catch(() => {
                let msg = 'Error reserving upload.'
                reject(msg)
                throw msg
              })
          },
          (err) => {
            reject(err)
          }
        )
      })
    },
    submitFile (file, options) {
      return new Promise((resolve, reject) => {
        let formData = new FormData()

        if (options === undefined) {
          options = {
            get_upload_link: true,
            is_public: true,
            active_forever: true,
            times_downloadable: 0
          }
        }

        formData.append('file_data', file)

        formData.append('get_upload_link', options.get_upload_link)
        formData.append('times_downloadable', options.times_downloadable)
        formData.append('active_forever', options.active_forever)
        formData.append('public', options.is_public)

        this.$http.post(this.uploadObject.file_upload.upload_url,
          formData,
          {
            withCredentials: true,
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }).then((resp) => {
            this.uploadObject = resp.data

            resolve(this.uploadObject)
          }).catch((err) => {
            let msg = 'Upload error.'
            if (err.response) {
              msg = err.response.data
            }
            reject(msg)
          })
      })
    },
    findUploadTypeIdx (uploadTypes, slug) {
      let idx = -1
      for (let i=0; i<uploadTypes.length; i++) {
        let t = uploadTypes[i]
        if (slug === t.slug) {
          idx = i
          break
        }
      }
      return idx
    },
    getBackendStats (baseUrl) {
      return new Promise((resolve, reject) => {
        let url = baseUrl + '/api/v1/_stats/'

        this.$http.get(url, {
          withCredentials: true
        }).then((res) => {

          resolve(res.data)

        }).catch((err) => {
          console.log('err', err)
          reject('Error fetching backend stats.')
        })
      })
    },
    getBackendInstanceStats (baseUrl) {
      /*
       Returns valid backend instance stats
       */
      let err_msg = 'Unable to find valid backend instance.'

      return new Promise((resolve, reject) => {
        this.getBackendStats(baseUrl).then(
          (backendStats) => {
            if (backendStats.status === 'green') {
              resolve(backendStats)
            } else {
              let backendUrl = backendStats.url
              let instances = backendStats.instances

              let backendId = -1
              for (let i=0; i<instances.length; i++) {
                let x = instances[i]
                if (x.url ===  backendUrl) {
                  backendId = i
                  break
                }
              }
              
              if (backendId > -1) {
                let nextBackendId = backendId + 1
                if (nextBackendId < instances.length) {
                  let nextInstanceDomain = instances[nextBackendId]['url']
                  this.getBackendInstanceStats(nextInstanceDomain).then(
                    (backendStats) => {
                      resolve(backendStats)
                    },
                    (err) => {
                      reject(err)
                    }
                  )
                } else {
                  reject(err_msg)
                }
              } else {
                reject(err_msg)
              }
            }
          },
          (err) => {
            reject(err)
          })
      })
    }
  }
}
